#include <stdio.h>

//in this function we're going to use recursion for 
//printing the reverse of digits.
void rev_digits(int num)
{
    printf("%d",num%10);
    if((num/10)!=0)
        rev_digits(num/10);
}

int main()
{
    int num;
    printf("Enter your num:  ");
    scanf("%d",&num);
    rev_digits(num);
    printf("\n");
}
 
