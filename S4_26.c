#include "stdio.h"

//function to return the size of the string
int size(char argv[])
{
  int n=0;
  while(argv[n]!=0){
    n++;
  }
  return n;
}

/* function to append the 2 strings
 * initiate entry from the index specified in the question */
void append(int in, char *argv[])
{
  int sz=size(argv[1])-in+ size(argv[2]);
  char str[sz];
  int n=0;
  while(argv[1][in]!='\0')
  {
    str[n++]=argv[1][in++];
  }
  for(int i=0;i<size(argv[2]);i++)
  {
    str[n++]=argv[2][i];
  }
  for(int i=0;i<n;i++)
    printf("%c",str[i]);
  printf("\n");
}

// function to return abcd and efghij as aebfcgdhij
void fix(char *argv[])
{
  //s1,s2 last index of str1 and str2  
  int s1=size(argv[1])-1;
  int s2=size(argv[2])-1;
    
  //sz=size of the appended string
  int sz=s1+s2+2;
  char str[sz];
    
  /* m=index of current element of str1
   * n=index of current element of str2
   * q=index of entries to be done */
  int m=0,n=0;
  int q=1;

  for(int i=0;i<sz;)
  {
    /* if n exceeds the last possible index of str2, store 
     * the elements of str1 */  
    if(n>s2) str[i++]=argv[1][m++];
    /* if m exceeds the last possible index of str1, store 
     * the elements of str2 */  
    else if(m>s1) str[i++]=argv[2][n++];
    /* else store them according to question */
    else
    {
      if(q%2==1) str[i++]=argv[1][m++];
      else str[i++]=argv[2][n++];
      q++;
    }
  }

  for(int i=0;i<sz;i++)
  {
    printf("%c", str[i]);
  }
  printf("\n");
}


int main(int argc, char *argv[]) {
  printf("Enter your index:   ");
  int in=0;
  scanf("%i",&in);
  
  printf("The appended string is::\t");  
  append(in,argv);
  
  printf("The appended string with elements taken alternatively::\t");
  fix(argv);
}
