#include <stdio.h>

//initializing the array elements
void input(int arr[],int N)
{
    for(int i=0;i<N;i++)
        scanf("%d",&arr[i]);
}

//the output is displayed
void output(int arr[],int N)
{
    for(int i=0;i<N;i++)
        printf("%d   ",arr[i]);
}

//the array is sorted (insertion sort)
void sort(int arr[], int N)
{
    int temp=0;
    for(int i=0;i<N;i++)
    {
        for(int j=i+1; j<N;j++)
        {
            if(arr[i]>arr[j])
            {
                temp=arr[i];
                arr[i]=arr[j];
                arr[j]=temp;
            }
        }
    }
}

int main()
{
    int N;
    printf("Enter the number of elements:  ");
    scanf("%d",&N);

    int arr[N];
    input(arr,N);
    sort(arr,N);
    output(arr,N);

}
