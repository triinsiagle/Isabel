#include "stdio.h"

int main()
{
  int n;
  printf("No of elements to input::\t");
  scanf("%i",&n);

  int list[n];
  for(int i=0;i<n;i++)
  {
    printf("list[%i]=",i+1);
    scanf("%i",&list[i]);
  }
  
  /*initiating max and min elements
   * to the first elements in the list*/
  int max=list[0];
  int min=list[0];

  for(int i=0;i<n;i++)
  {
    /*finding the max and min by iterating
     *throughout the loop */  
    if(max<list[i]) max=list[i];
    if(min>list[i]) min=list[i];
  }

  printf("Range=%i\n",max-min);
}
