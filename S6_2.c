#include <stdio.h>

unsigned char mask=128;

//function to print the number in binary
void print(int num)
{
  int t=mask;
  while(t!=0)
  {
    if((t&num)>0) printf("1");
    else printf("0");
    t=t>>1;
  }
  printf("\n");
}

//function to recursively shift and add bits
void shift(int num, int n)
{
  if((num&1)>0)
  {
    num=num>>1;
  }
  else
  {
    num=num>>1;
    num=num|mask;
  }

  if(--n>0) shift (num,n);
  else print(num);
}

int main()
{
  unsigned int x;
  printf("Enter the number(0-255)::\t");
  scanf("%i",&x);

  while((x&mask)<=0 && mask!=0)
  {
    mask=mask>>1;
  }

  int n;
  printf("Enter the number of bits to shift::\t");
  scanf("%i",&n);

  shift(x,n);
}
