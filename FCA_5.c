#include <stdio.h>

int main()
{
    int N;
    printf ("Enter the number until which you wanna "
            "find Prime numbers:  ");
    scanf ("%d",&N);

    int arr[N-2];
    //initializing the array elements to zero
    for(int i=0; i<N-2;i++) 
        arr[i]=0;
    //initializing those elements to 1 which aren't primes 
    for(int i=2; i*i<N-2;i++)
    {
        if(arr[i-2]==0)
        {
            for(int j=i*2-2;j<N-2;j=j+i)
                arr[j]=1;
        }
    }
    //printing just the prime numbers
    for (int i=0;i<N-2;i++)
    {
        if(arr[i]==0)
            printf ("%d\n",i+2);
    }

    return 0;
}
