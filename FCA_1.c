/*Write an algorithm to determine maximum of three numbers. Also draw the corresponding flow chart.*/

#include <stdio.h>

int main()
{
    int N;
    printf("Enter the number of elements: ");
    scanf ("%d",&N);

    int arr[N];

    for(int i=0;i<N;i++)
    {
        scanf("%d",&arr[i]);   //here the array elements are initialized
    }                          //to their respective values

    int max=arr[0];

    for(int i=0; i<N;i++)
    {
        if(arr[i]>max)      //here we're updating the max element
            max=arr[i];
    }

    printf("max= %d\n",max);
}
