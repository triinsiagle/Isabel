#include "iostream"
using namespace std;

void merge(int a1[],int a2[], int s1, int s2)
{
  int arr[s1+s2];
  int st1=0;
  int st2=0;

  for(int i=0;i<s1+s2;i++)
  {
    if(st1>s1-1) arr[i]=a2[st2++];
    else if(st2>s2-1) arr[i]=a1[st1++];
    else if(a1[st1]>a2[st2]) arr[i]=a1[st1++];
    else arr[i]=a2[st2++];
  }

  for(int i=0; i<s1+s2;i++)
    cout<<arr[i]<<" ";
}


int main()
{
  int T;
  cin>>T;
  while(T-->0)
  {
    int s1,s2;
    cin>>s1>>s2;
    int a1[s1],a2[s2];

    for(int i=0;i<s1;i++)
    {
      cin>>a1[i];
    }
    for(int i=0;i<s2;i++)
    {
      cin>>a2[i];
    }

    merge(a1,a2,s1,s2);

    cout<<"\n";

  }
}
