#include <stdio.h>

char out(int n)
{
	switch(n)
	{
		case 0: return '0';
		case 1: return '1';
		case 2: return '2';
		case 3: return '3';
		case 4: return '4';
		case 5: return '5';
		case 6: return '6';
		case 7: return '7';
		case 8: return '8';
		case 9: return '9';
		case 10: return 'a';
		case 11: return 'b';
		case 12: return 'c';
		case 13: return 'd';
		case 14: return 'e';
		case 15: return 'f';
	}
}

int inp(char a)
{
	switch(a)
	{
		case '0': return 0;
		case '1': return 1;
		case '2': return 2;
		case '3': return 3;
		case '4': return 4;
		case '5': return 5;
		case '6': return 6;
		case '7': return 7;
		case '8': return 8;
		case '9': return 9;
		case 'a': return 10;
		case 'b': return 11;
		case 'c': return 12;
		case 'd': return 13;
		case 'e': return 14;
		case 'f': return 15;
	}
}

void print(int n, int t)
{
	if(n/t!=0)
	print(n/t,t);
	printf("%c",out(n%t));
}

int input(int ch)
{
	printf("Enter the number of digits: ");
	int n;
	scanf("\n");
	scanf("%d",&n);

	char a[n];
	int i;
	int b=1;
	int num=0;
  scanf("\n");
	for(i=n-1; i>=0; i--)
	    {
				 scanf("%c",&a[i]);
			 }

  for(i=0; i<n; i++)
	{
		int temp = inp(a[i]);
		num=num+ temp*b;
		b=b*ch;
	}

	return num;

}

int main()
{
	int tp;
	printf("Enter the type of input:\n 16 for hex\n 2 for binary\n 8 for octal\n 10 for decimal::  ");
	scanf("%d",&tp);

	int n=input(tp);

	printf("Enter the type of output:\n 16 for hex\n 2 for binary\n 8 for octal\n 10 for decimal::  ");
	int ch;
	scanf("%d",&ch);

	switch (ch) {
		case 2: print(n,2); break;
		case 16: print(n,16);  break;
		case 8: print(n,8);  break;
		case 10: printf("%d", n );
	}

}
