#include <stdio.h>

int main()
{
    int n;

    printf ("Enter the number of elements you wanna print:  ");
    scanf  ("%d",&n);

    int p=0,q=1,temp=0;
    
    //here we keep a temporary variable so as to store the data
    //of unchanged variable p,
    //p->the second last element, q-> last element
    for(int i=1;i<=n;i++)
    {
        if(i==1) printf ("0, ");
        else if (i==2) printf ("1, ");
        printf ("%d, ",p+q);
        temp=p;
        p=q;
        q=temp+q;
    }
    printf ("\n");
}
