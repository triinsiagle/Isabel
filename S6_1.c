#include <stdio.h>

int main()
{
  unsigned char ch;
  unsigned char mask =128;

  printf("Enter the character:: \t");
  scanf("%c",&ch);

  if((ch&mask)>0) printf("Female\n");
  else printf("Male\n");
  mask=mask>>1;
  
  //b is the bit value to be added  
  int yr=0;
  int b=0;

  for(int i=0; i<2;i++)
  {
    if((ch&mask)>0) b=1;
    else b=0;
    yr=yr*2+b;
    mask=mask>>1;
  }
  printf("Year=\t%i\n",yr+1);

  int age=0;
  b=0;

  for(int i=0;i<5;i++)
  {
    if((ch&mask)>0) b=1;
    else b=0;
    age=age*2+b;
    mask=mask>>1;
  }
  printf("Age=\t%i\n",age);

}
