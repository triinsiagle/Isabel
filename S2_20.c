#include <stdio.h>

int main()
{
    int n;

    printf ("Enter the perfect square:  ");
    scanf  ("%d",&n);

    int sqr_n=0;
    int i=1;
    
    //here we keep a record of the number of iterations necessary
    //to make the perfect square using the odd numbers.
    //no. of iterations=the square root of the number
    while (n!=0)
    {
        sqr_n++;
        n=n-i;
        i=i+2;
    }

    printf ("The perfect square is:  %d\n",sqr_n);

}
