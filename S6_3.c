#include <stdio.h>

unsigned char mask=128;

//here we print the number
void print(unsigned int x)
{
  int num=0;
  int a=1;
  while(mask!=0)
  {
    if((mask&x)>0) num=num+a*1;
    a=a*2;
    mask=mask>>1;
  }
  printf("num=\t%i\n",num);
}


int main(void)
{
  unsigned int x;
  printf("Enter the number(0-255)::\t");
  scanf("%i",&x);
  
  //set the mask according to the number  
  while((x&mask)<=0 && mask!=0)
  {
    mask=mask>>1;
  }

  print(x);
}
