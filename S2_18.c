#include <stdio.h>

//here we find the sum of the cubes of the digits
int cubesum(int n,int sum)
{
    if(n/10!=0)
        cubesum (n/10,sum+(n%10)*(n%10)*(n%10));
    else return sum+(n%10)*(n%10)*(n%10);
}

int main()
{
    int range;

    printf ("Enter the range in which you "
            "wanna find Armstrong number:");
    scanf  ("%d",&range);

    //here we check whether a number is Armstrong number 
    //or not and print accordingly
    for(int i=1;i<=range;i++)
    {
        if(cubesum (i,0)==i)
            printf ("%d\n",i);
    }
}
