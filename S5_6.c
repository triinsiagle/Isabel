#include <stdio.h>
#include <stdlib.h>

struct stack
{
  int val;
  struct stack *next;
};

//Here we add the elements of the stack`from the head
void push_back(int val, struct stack **head)
{
  //creation of pointer temp to store the location of a stack variable  
  struct stack *temp=(struct stack*)malloc(sizeof(struct stack));
  
  //assigning the value of members the variable pointed by temp
  temp->val=val;
  temp->next=(*head);
  
  //updating the head to the new variable
  *head=temp;
}

void pop_back(struct stack **head)
{
   //just making head point to the new element in the stack 
   *head=(*head)->next;
}

void console(struct stack *head)
{
  //we traverse the stack until we reach a element pointing nowhere  
  while(head!=NULL)
  {
    printf("%i\t",head->val);
    
    //we go to the next element here
    head=head->next;
  }
  printf("\n");
}



int main()
{
  char y;
  struct stack *head= (struct stack*)malloc(sizeof(struct stack));

  printf("Enter y if you want to input more:\n"
          "Enter the input::\t ");
  scanf("%i",&(head->val));
  head->next=NULL;

  printf("Do you want to input more::\t");
  scanf(" %c",&y);
  console(head);
  int val;
  while(y=='y')
  {
    printf("Enter your input::\t");
    scanf("%i",&val);
    push_back(val,&head);
    console(head);

    printf("Do you want to input more::\t");
    scanf(" %c",&y);
  }

  printf("Do you want to popback?\t");
  scanf(" %c",&y);
  
  while (y=='y')
   {
     pop_back(&head);
     console(head);
     printf("Do you want to popback?\t");
     scanf(" %c",&y);
     
   }
}
