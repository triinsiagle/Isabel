#include <stdio.h>
#include <math.h>


struct Point
{
  float x;
  float y;
};

//function to calculate the distance between two points
float dist(struct Point p1, struct Point p2)
{
  float d2 = pow(p1.x-p2.x,2)+pow(p1.y-p2.y,2);
  return sqrt(d2);
}

/* function to calculate the area enclosed by the 
 * three points p1, p2 and p3 */
float Area(struct Point p1, struct Point p2, struct Point p3)
{
  float a=dist(p1,p2);
  float b=dist(p2,p3);
  float c=dist(p3,p1);

  float s=(a+b+c)/2;

  float area= s*(s-a)*(s-b)*(s-c);
  return sqrt(area);
}

int main(void)
{
  struct Point p1,p2,p3,p;

  printf("Enter the co-ordinates of Point 1,2,3::\n");
  scanf("%f %f",&(p1.x),&(p1.y));
  scanf("%f %f",&(p2.x),&(p2.y));
  scanf("%f %f",&(p3.x),&(p3.y));

  printf("Area=\t%f\n",Area(p1,p2,p3));

  printf("Enter the co-ordinate of the fourth point::\t");
  scanf("%f %f",&(p.x),&(p.y));

  float A=Area(p1,p2,p3);
  float A1=Area(p,p1,p2);
  float A2=Area(p,p2,p3);
  float A3=Area(p,p1,p3);
  
  /* if the point is inside the triangle
   * then the net area = summation of the areas enclosed
   * by the sub triangles */  
  if(A==(A1+A2+A3)) printf("1\n");
  else printf("0\n");

}
