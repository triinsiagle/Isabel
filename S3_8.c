#include "stdio.h"

int main()
{
  int m;

  printf("Enter the index::    ");
  scanf("%i",&m);
  int mat[m][m];
  
  //taking input in the m x m matrix  
  for(int i=0;i<m;i++)
  {
    for(int j=0;j<m;j++)
    {
      printf("mat[%i][%i]= ",i,j);
      scanf("%i",&mat[i][j]);
    }
  }
  
  //displaying the matrix that entered by user
  for(int i=0;i<m;i++)
  {
    for(int j=0;j<m;j++)
    {
      printf("%i\t",mat[i][j]);
    }
    printf("\n");
  }
  
  //temp=variable to hold the value of a[i] temporarily
  int temp;
  printf("\n\n");
  
  /*swapping the values with their w.r.t their
   *reflections along the main diagonal*/  
  for(int i=0;i<m;i++)
  {
    for(int j=i+1;j<m;j++)
    {
      temp=mat[i][j];
      mat[i][j]=mat[j][i];
      mat[j][i]=temp;
    }
  }
  
  //displaying the matrix that is transposed
  for(int i=0;i<m;i++)
  {
    for(int j=0;j<m;j++)
    {
      printf("%i\t",mat[i][j]);
    }
    printf("\n");
  }

}
