#include <stdio.h>
int sum=0;      //sum is initialized to zero globally

//this function calculates the sum of digits
void sum_digits(int num)  
{
    if((num/10)!=0)
        sum_digits(num/10);     //recursively calling till we are left with
    sum=sum+num%10;             //one digit.
}

int main()
{
    int num;
    printf("Enter your num:  ");
    scanf("%d",&num);
    sum_digits(num);
    printf("%d\n",sum);    
}
