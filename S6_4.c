#include <stdio.h>
#define no_of_bits 8*sizeof(unsigned int)

int main()
{
  unsigned int x,y,z=0;

  printf("Enter the value of x::\t");
  scanf("%du",&x);
  printf("Enter the value of y::\t");
  scanf("%du",&y);

  unsigned int mask=1;

  for(int i=1;i<=(no_of_bits)/2;i++)
  {
    if((x&mask)>0) z=z|mask;
    mask = mask<<1;
    if((y&mask)>0) z=z|mask;
    mask = mask<<1;
  }

  printf("The new integer is::\t%i\n",z);

}
