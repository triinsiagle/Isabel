#include <stdio.h>

//In this function we're gonna check for primality of the number
int primality_check(int N)
{
    //looping till square root of the number
    for(int i=2; i*i<=N;i++) 
    {
        if(N%i==0)  
            return 0;
    }
    return 1;    
}

int main()
{
    int N;
    printf("Enter your number:  ");
    scanf("%d",&N);
    if(primality_check(N)) printf("Is prime\n");
    else printf("Not prime\n");
    return 0;
}
