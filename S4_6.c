#include <stdio.h>

/* function to print the array in reverse order
 * using recursion */

void reverse(char a[])
{
  if(a[1]!='\0')
    reverse(a+1);
  printf("%c\t",a[0]);
}

int main(int argc, char *argv[])
{
  for(int i=0;i<argc;i++)
  {
    reverse(argv[i]);
    printf("\n");
  }
}
