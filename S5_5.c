#include "stdio.h"
#include "stdlib.h"

struct Node
{
  int data;
  struct Node *address;
};

// Creating a node
void create(int val, struct Node **head)
{
  struct Node *temp=(struct Node*)malloc(sizeof(struct Node));
  /*We created a pointer which points to the memory location( returned by    
   *malloc) which is Node datatype. */
  temp->data=val;
  /* access the structure member data of the variable pointed by temp
   * and store data there */ 
  temp->address=(*head);
  /* access the struct member address of the variable pointed by temp
   * and store the location of the next pointer */
  (*head)=temp; // make the node as the new head
}

//function to output our linked list
void console_o(struct Node *n)
{
  while(n!=0)
  {
    printf("%i\t",n->data);
    n=n->address; // after printing data go to the new node
  }
  printf("\n");
}

//insert an element after node n
void insert(struct Node **n,int val)
{
  struct Node *temp=(struct Node*)malloc(sizeof(struct Node));
  temp->address=(*n)->address; 
  /* make the address stored inside the variable the location 
   * of next node */
  temp->data=val;
  (*n)->address=temp;
  /* update the location of the n to
   * the location of temp */ 
}

//function to delete any node after head
void delete(struct Node **n)
{
  struct Node *temp=(struct Node*)malloc(sizeof(struct Node));
  
  /* Skip the immediate node after n and 
   * store the location of that node in pointer temp */
  temp=(*n)->address;
  temp=temp->address;
  
  //update n's address to the location pointed by temp
  (*n)->address=temp;
}

//Reversing the whole list 
void reverse(struct Node **head)
{
  /* prev points to the location of last node accessed
   * current is pointing the location of current node
   * next is pointing to the location of the next node */  
  struct Node *prev=NULL;
  struct Node *current=*head;
  struct Node *next=NULL;
  
  while(current!=NULL)
  {      
    next=current->address; 
    current->address=prev; //changing the address of the pointer from next to prev
    prev=current;          //updating the prev
    current=next;          //updating the current
  }
  *head=prev;
}



int main()
{
  char more;
  int val;
  struct Node *head=(struct Node*)malloc(sizeof(struct Node));
  printf("Enter the value: ");
  scanf("%i",&val);
  head->data=val;
  head->address=0;
  printf("Do you wanna input more data: Input 'y' for yes:\t");
  scanf(" %c",&more);
  while(more=='y')
  {
    printf("Enter the value: ");
    scanf("%i",&val);
    create(val,&head);
    console_o(head);

    printf("More data::\t");
    scanf(" %c",&more);
  }
  
  printf("Reversing the order::\n");
  reverse(&head);
  console_o(head);

  char want;
  printf("Do you want to add more data? Press 'y' for yes::\t");
  scanf(" %c",&want);


  while(want=='y')
  {
    int pos;
    printf("Enter the position::\t");
    scanf("%d",&pos);

    struct Node *n=head;
    while(pos-->0)
      n=n->address;

    int val;
    printf("Enter the value::\t");
    scanf("%i",&val);
    insert(&n,val);

    console_o(head);

    printf("More data::\t");
    scanf(" %c",&want);
  }

  printf("Do you want to delete nodes?::\t");
  scanf(" %c",&want);

  while(want=='y')
  {
    int pos;
    printf("Enter the position::\t");
    scanf("%d",&pos);

    struct Node *n=head;
    while(pos-->1)
      n=n->address;
    delete(&n);
    console_o(head);

    printf("Do you want to delete nodes?::\t");
    scanf(" %c",&want);
  }
}
