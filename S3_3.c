#include "stdio.h"

//we take input in a 3x3 matrix
void input(int mat[3][3])
{
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++)
    {
      printf("mat[%i][%i]= ",i+1,j+1);
      scanf("%i",&mat[i][j]);
    }
  }
}

//we display the 3x3 matrix
void output(int mat[3][3])
{
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++)
    {
      printf("%4i ",mat[i][j]);
    }
    printf("\n");
  }
  printf("\n\n");
}

/* Finding the sum of two matrix
 * we find the sum iteratively between two 
 * corresponding elements */
void sum(int mat1[3][3],int mat2[3][3])
{
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++)
    {
      mat1[i][j]=mat1[i][j]+mat2[i][j];
    }
  }
  output(mat1);
}

/*Substracting mat1[][]-mat2[][]
 * the same way is addition */
void substract(int mat1[3][3],int mat2[3][3])
{
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++)
    {
      mat1[i][j]=mat1[i][j]-mat2[i][j];
    }
  }
  output(mat1);
}

/*Multiplying (row x column)
 * using 3 iterations to multiply traditionally */
void product(int mat1[3][3],int mat2[3][3])
{
  int mat[3][3];
  for(int r=0;r<3;r++)
  {
    for(int c=0;c<3;c++)
    {
      mat[r][c]=0;
      for(int i=0;i<3;i++)
      {
        mat[r][c]=mat[r][c]+mat1[r][i]*mat2[i][c];
      }
    }
  }
  output(mat);
}

int main() {
  char ch;
  printf("Enter what you wanna do with the matrices\n"
         "Enter s for sum\n"
         "Enter d for substraction(maintain order of d=A-B\n"
         "Enter p for product(maintain order of p=A*B\n");
  scanf(" %c",&ch);


  printf("Enter the matrix A::\n");
  int mat1[3][3];
  input(mat1);
  printf("The matrix entered is\n");
  output(mat1);

  printf("Enter the matrix B::\n");
  int mat2[3][3];
  input(mat2);
  printf("The matrix entered is\n");
  output(mat2);



  switch(ch)
  {
    case 's': sum(mat1,mat2); break;
    case 'd': substract(mat1,mat2); break;
    case 'p': product(mat1,mat2); break;
    default: printf("Input invalid\n");
  }


}
