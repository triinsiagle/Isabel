#include <stdio.h>

//here we calculate the sum of the factors
int factor_sum(int n)
{
   int sum=0;
   for(int i=1;i*i<=n;i++)
   {
       if(n%i==0)
       {
          if(i*i!=n) sum=sum+i+(n/i);
          else sum=sum+i;
       }
   }

   return sum;
}

int main()
{
    int n,sum=0 ;

    printf ("Enter the range of factor sum summation:  ");
    scanf  ("%d",&n);
    
    //here we iterate to find the summation of the numbers
    while(n>0)
        sum=sum+factor_sum (n--);

    printf ("The summation of the series is: %d\n",sum);

    return 0;
}
