#include <stdio.h>

int N=10;

//here we find out the number of digits in the second number
int nofdigits (int n)
{
    if((n/10)!=0)
    {
        N=N*10;
        nofdigits (n/10);
    }
    else return N;
}

int main()
{
    int N1, N2;
    printf ("Enter the two numbers:  ");
    scanf ("%d %d",&N1,&N2);
    //N1*(10^no_of_digits(N2))+N2=appended sequence
    printf ("The appended sequence is: %d\n",N1*nofdigits (N2)+N2);
}
