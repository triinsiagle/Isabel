#include <stdio.h>

//here we find the sum of the square of the digits
int sqrd_sm(int n,int sum)
{
    if(n/10!=0)
        sqrd_sm (n/10,sum+(n%10)*(n%10));
    else return sum+(n%10)*(n%10);
}


int main()
{
    int n;

    printf ("Enter the number of happy "
            "numbers you wanna search: ");
    scanf  ("%d",&n);

    int num=1;

    while(n>0)
    {
        //a temp variable is used to store the value of sqrd_sm
        //which serves as next input
        int temp=num;
        //here we iterate 100 times to find if the number is a happy number
        for(int i=0;i<100;i++)
        {
            //condition for happy number is sum of squared digits=1 
            if(sqrd_sm (temp,0)==1)
            {
                printf ("%d\n",num);
                n--;
                break;
            }
            else
                temp=sqrd_sm (temp,0);
        }
        //we update the number to be used
        num++;
    }

}
