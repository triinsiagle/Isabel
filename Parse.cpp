#include <iostream>
#include <vector>
using namespace std;

void numgen(vector<char> num)
{
    int rad=10;
    float n=0;
    int i=0;
    while(num[i]!='.'&&i<num.size())
    {
        n=n*10+(num[i]-'0');
        i++;
    }
    for(i=i+1;i<num.size();i++)
    {
        n=(n*rad+(num[i]-'0'))/rad;
        rad=rad*10;
    }
    cout<<n<<"\n";
}

int main()
{
    vector<char> input;
    vector<char> num;
    char ch;
    ch=cin.get();
    while(ch!='\n')
    {
        input.push_back(ch);
        ch=cin.get();
    }

    for(int i=0;i<input.size();i++)
    {
        bool dig=false;
        while (i<input.size()&&((input[i]>='0' && input[i]<='9')||input[i]=='.'))
        {
            dig=true;
            num.push_back(input[i]);
            i++;
        }
        if(dig)
        {
            numgen(num);
            num.clear();
        }
    }

}
