#include <stdio.h>

//A*x*x+B*x+C is the general quadratic equation
float A,B,C;

float p(float x)
{
    return A*x*x + B*x +C;
}

//function to difference of two numbers
float absubs(float a , float b)
{
    if((a-b)>0) return a-b;
    else return b-a;
}

//function to calculate the root with an accuracy of 0.0000001
//function uses recursive bisection method
float root_es(float a, float b)
{
    float m=(a+b)/2;
    if(p(m)==0.0) return m;
    else if(absubs(a,b)<0.0000001) return m;
    else if(p(m)*p(a)<0) root_es(m,a);
    else if(p(m)*p(b)<0) root_es(m,b);
}

int main()
{
    printf("Enter the values of A,B,C:  ");
    scanf("%f%f%f",&A,&B,&C);
    float c= -B/(2*A);

    float r1=0;

    if(B*B<4*A*C)
        printf("No roots..");
    else
    {
        if(p(0)*p(c)<0) r1= root_es(0,c);
        else
        {
            float x0=(p(0)*c)/(p(0)-p(c));
            r1 = root_es(x0,c);
        }
        printf("%f\t%f\n",r1,2*c-r1);
    }
}
